function myFunction(x) {
    x.classList.toggle("change");
}
function drawer() {
    var element = document.getElementById("drawer");
    var target  = document.getElementById("drawer-contents");
    element.addEventListener("click", function() {
        if (target.className === "drawer-open") {
            target.className = 'drawer-close';
        } else {
            target.className = 'drawer-open';
        }
    }, false);
    return false;
}
window.onload = drawer;